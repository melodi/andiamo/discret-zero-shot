# DiscReT Zero-Shot

This repository hosts the code for the paper "Zero-shot learning for multilingual  discourse relation classification" by Eleni Metheniti, Philippe Muller, Chloé Braud, and Margarita Hernández-Casas.

Preprint available in HAL: [https://hal.science/hal-04483805](https://hal.science/hal-04483805)

## Datasets

The datasets come from the DISRPT 2023 Shared Task 3: [Discourse Relation Classification across Formalisms](https://sites.google.com/view/disrpt2023/home#h.lkwe16jjm90). The repository for the data can be found on [Github](https://github.com/disrpt/sharedtask2023/tree/v1.0). (Note that some datasets are only made available by owning a version of non-open source corpora, such as PDTB 3.0 (Prasad et al., 2019). Please refer to the README files of each dataset in the Shared Task repository.)

After cloning the repo and converting the underscored files, either copy the ```data``` folder to this repo's main folder, or point to the data folder with the ```--data_path``` argument.

The full list of datasets with statistics: [here](https://github.com/disrpt/sharedtask2023/tree/v1.0?tab=readme-ov-file#statistics).


## Prerequisites
* torch (tested on 1.12 with CUDA)
* transformers
* scikit-learn

Install requirements with ```pip install -r requirements.txt```.

## Run 

### Make the mappings

First, make the mappings for the training set, accaroding to the datasets that will be used for the training:
```
python make_mappings_zero-shot.py \
	--langs_to_use [LIST OF DATASETS IN ONE STR SEPARATED BY ;] \
	--mappings_file [NAME TO SAVE THE MAPPINGS]
```
* If you want to train with all datasets, leave ```langs_to_use``` emtpy (default value is all datasets, ```@```).
* The mappings file will be saved by default in the ```mappings``` folder and is in .tsv format.

Example, to train only with the German (deu.rst.pcc) and Spanish (spa.rst.rstdt, spa.rst.sctb) datasets:
```
python make_mappings_zero-shot.py \
	--langs_to_use "deu.rst.pcc;spa.rst.rstdt;spa.rst.sctb" \
	--mappings_file "mappings_deu_spa.tsv" 
```

### Run the classifier
```
python classifier_pytorch.py \
	--langs_to_use [LIST OF DATASETS IN ONE STR SEPARATED BY ;] \
	--mappings_file [NAME TO SAVE THE MAPPINGS]
```
Additional arguments and defaults:
```
	--tranformer_model "bert-base-multilingual-cased" \
	--num_epochs 10 \
	--batch_size 8 \
	--gradient_accumulation_steps 16 \
	--use_cuda "yes"
```
## References 


Metheniti, Eleni, Muller, Philippe, Braud, Chloé, & Hernández-Casas, Margarita. (2024). Zero-shot learning for multilingual discourse relation classification. ⟨hal-04483805⟩

Prasad, Rashmi, Webber, Bonnie, Lee, Alan, & Joshi, Aravind. (2019). Penn Discourse Tree
bank Version 3.0 [Data set]. Linguistic Data Consortium. https://doi.org/10.35111/QEBF-GK47