#!/usr/bin/env python
# coding: utf-8

import os
from configure import parse_args
from utils import *

args = parse_args()


# -----------------------------------
# open substitutions per file
mappings = {}
with open("mappings/mappings_substitutions.tsv", "r", encoding="utf-8") as f:
    next(f)
    for line in f:
        l = line.strip().split("\t")
        mappings[l[0]] = l[1]


# find the labels that were changed
inv_mappings = {}
subs = {}
for label, num in mappings.items():
    if not num in inv_mappings:
        inv_mappings[num] = label
    else:
        subs[label] = inv_mappings[num]


# -----------------------------------
# define which language to use with the arguments
languages = args.langs_to_use.split(";")


corpora = [
    folder
    for folder in os.listdir(args.data_path)
    if any(l in folder for l in languages)
]

files = [
    "/".join([args.data_path, corpus, f])
    for corpus in corpora
    for f in os.listdir(args.data_path + "/" + corpus)
]


# open the files
def read_file(file):
    """Open the relations file."""
    relations = []
    sub_rels = []
    with open(file, "r", encoding="utf-8") as f:
        next(f)
        for line in f:
            try:
                l = line.strip().split("\t")
                if not l[11].lower() in subs:
                    relations.append(l[11].lower())
                else:
                    sub_rels.append(l[11].lower())
            except IndexError:
                pass
        return relations, sub_rels


rel_files = [f for f in files if any(x in f for x in ["train"])]

good_rels = []
sub_rels = []
for f in rel_files:
    x, y = read_file(f)
    good_rels += x
    sub_rels += y

dict_labels = dict(enumerate(list(set(good_rels))))
corpora_labels = {v: k for k, v in dict_labels.items()}


leftovers = []

for sub in sub_rels:
    try:
        corpora_labels[sub] = corpora_labels[subs[sub]]
    except KeyError:
        corpora_labels[subs[sub]] = max(list(corpora_labels.values())) + 1
        corpora_labels[sub] = corpora_labels[subs[sub]]

corpora_labels["unk"] = max(list(corpora_labels.values())) + 1

with open("mappings/" + args.mappings_file, "w") as f:
    f.write("LABEL\tMAPPING\n")
    for k, v in corpora_labels.items():
        f.write(k + "\t" + str(v) + "\n")
